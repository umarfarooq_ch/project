//============================================================================
// Name        : bubble-shooter.cpp
// Author      : Muhammad Umar Farooq - i15-01018
// Version     : v7.5.2
// Copyright   : (c) Reserved
// Description : Basic 2D game of Bubble Shooting...
//============================================================================
#ifndef BUBBLE_SHOOTER_CPP
#define BUBBLE_SHOOTER_CPP

#include <GL/gl.h>
#include <GL/glut.h>
#include <iostream>
#include<string>
#include<cmath>
#include "util.h"
using namespace std;
#define MAX(A,B) ((A) > (B) ? (A):(B)) // defining single line functions....
#define MIN(A,B) ((A) < (B) ? (A):(B))
#define ABS(A) ((A) < (0) ? -(A):(A))
#define FPS 30

#define KEY_ESC 27 // A

// 20,30,30
const int bradius = 25; // ball radius in pixels...




//....Prototypes...
int GetColor();
float Distance(float x1, float y1, float x2, float y2);
void Pixels2Cell(int px, int py, int & cx, int &cy);
void Cell2Pixels(int cx, int cy, int & px, int &py);
void store_balls_coordinates();
void store_color_array();
void initializing_states();
void color_to_white(int row, int col);
void level_line_increase();
int theta_calculater();
int theta_calculater_for_increment();
int theta_calculater_for_increment_1();
int theta_calculater_for_increment_2();
void calculate_distances ();
int GAME_END_Calculater();
void Call_Burst();
void Move_Bubble();
void Burst(int gol_row,int gol_col,int &n);
bool Collision_Detection ()	;
bool last_collision(int row);
void Display();
void SetCanvasSize(int width, int height);
void NonPrintableKeys(int key, int x, int y);
void MouseMoved(int x, int y);
void MouseClicked(int button, int state, int x, int y);
void PrintableKeys(unsigned char key, int x, int y);
void Timer(int m);
void comaprison_initializer();
void ball_line_initiaizer();
int  ball_line_increment();
//.....


int width = 810, height = 600; // i have set my window size to be 800 x 600
int dball = 0; // difference between balls center of 5 pixels
int byoffset = 3 * bradius + dball; // board yoffset

float score = 0;

/*
 * Main Canvas drawing function.
 * */

int setcolor, psetcolor;
enum GameState { // Use to check different states of game...
	Ready, Shot, Over, RemoveCluster
};
GameState gamestate = Ready;

int GetColor() {
	return GetRandInRange(7, 16) * 10;
}
int neighbors[][6][2] = { { { 0, 1 }, { 0, -1 }, { -1, -1 }, { -1, 0 },
		{ 1, -1 }, { 1, 0 } }, // Even row tiles
		{ { 0, 1 }, { 0, -1 }, { -1, 0 }, { -1, 1 }, { 1, 0 }, { 1, 1 } } }; // Odd row tiles

// Function returns a random number from following set [0,10,20,30,40,50,60,70]
// Replace 8 with smaller number if you want small set...
// Calculate L2 distance between two points...
float Distance(float x1, float y1, float x2, float y2) {
	return sqrt((x2 - x1) * (x2 - x1) + (y2 - y1) * (y2 - y1));
}
void Pixels2Cell(int px, int py, int & cx, int &cy)
// converts the Pixel coordinates to cell coordinates...
		{

}
void Cell2Pixels(int cx, int cy, int & px, int &py)
// converts the cell coordinates to pixel coordinates...
		{
}

//data storages :P
int array_data [10][13][4]; // {ball_row , ball_col, {color or state,position x-axis,position y-axis,original_color}
int shoot_ball_data [4];		// {where x to be targeted, same y, color before shot, color of moving ball}
int mouse_axis_saving[4];	// {move axis X, moving axis Y, clicked axis X, clicked Axis Y}
int v_of_lli=0;				// this variable stands for : variable of level_line_increase:|
int v_of_lli_usedin_mouseclicked = 0;
double increments_ball_shoot[2]; // it will store much increment should be in x and y;
int global_color,row_global,col_global,global_fix1_1=0; // all these variables are used for spot of ball when it collide
int temp_array[2]={400,40};
int wall_collision=0;
int array_compare[100];
int temp11[1] = {0};
int SCORE_1 =0;
int Game_Code[1] = { 0};
int ball_line_array[4]; // this array is storing 3 balls before shoot
int ball_change_counter = 3;
int Display_message = 1; //Condition to check display message at start of the game

void store_balls_coordinates() // this function is storing x,y coordinates of each ball in data_array
{
	int inc = 1*60;
	int i=0;
	for (int y=570;y>inc;y-=105)
	{
		int j =0;
		for (int x=30;x<800;x+=60)
		{
			array_data[i][j][1]=x;
			array_data[i][j][2]=y;
			j++;
		}
		i+=2;
	}
	int i1=1;
	for (int y=518;y>inc;y-=105)
	{
		int j=0;
		for (int x=60;x<800;x+=60)
		{
			array_data[i1][j][1]=x;
			array_data[i1][j][2]=y;
			j++;
		}
		i1+=2;
	}
}

void store_color_array() // storing color data to 3rd index in 3rd dimension of array_data
{
	for (int i6=0; i6<10; i6++)
	{
		for (int j6=0; j6<13; j6++)
		{
			array_data[i6][j6][3]= GetRandInRange(1, 10) * 10;
		}
	}
}

void initializing_states()
{
	for (int row =0; row<3; row++)
	{
		for (int col=0; col<13; col++)
		{
			array_data[row][col][0]=array_data[row][col][3];
		}
	}
	// remaining will changed to NULL state(white color)

	for (int row =3; row<10; row++)
	{
		for (int col=0; col<13; col++)
		{
			color_to_white (row,col);
		}
	}
}


void color_to_white(int row, int col) // function to change state by passing matrix points(tiles location)
{
	array_data [row][col][0] = 138; //white color = 138;
}
void level_line_increase() // this function when call, will increase a line on game
 {
	int temp_array [10][13][4]; 	// declaring a temporary array
	 for (int col=0; col < 13 ; col++)	//this entire loop is copying data of third dimension from array_data to temp_array
	 {
		 for (int row=0; row<10; row++)
		 {

			 for (int n=0; n<4; n++) // 3rd dimension is saving in temp array
			 {
						temp_array[row][col][n] = array_data[row][col][n];
			 }
		 }
	 }

	 //..........123.......... this conditions and loops are transferring last line date to first line
	 if (v_of_lli%2==0)
	 {

		 for (int i=0; i<13; i++) // Transferring last line colors to first line
		 {
			 array_data[0][i][3] = temp_array[9][i][3]; // real color copied
			 array_data[0][i][0] = temp_array[9][i][3]; // state changed from null to visible by passing values from 3rd index to 0 index
			 array_data[0][i][1] += 30;
		 }
	 }
	 else
	 {
		 for (int i=0; i<13; i++) // Transferring last line colors to first line
		 {
			 array_data[0][i][3] = temp_array[9][i][3];
			 array_data[0][i][0] = temp_array[9][i][3]; // state changed from null to visible by passing values from 3rd index to 0 index
			 array_data[0][i][1] -= 30;
		 }
	 }
	 //.........end of 123..........


	 //..........abc123.............  this whole system is dealing with remaining rows by transfering temp_array to array_data
		 for (int row=1; row < 10 ; row++)
		 {
			 for (int col=0; col<13; col++)
			 {

				 	 	 if (v_of_lli%2==1)
					 	 {


							 	 array_data[row][col][3] = temp_array[row-1][col][3];
							 	 array_data[row][col][0] = temp_array[row-1][col][0]; //i m copying same state because if ball is hide and level will increase then it should maintain the same STATE
							 	 array_data[row][col][1] += 30;

					 	 }
					 	 else
					 	 {

							 	 array_data[row][col][3] = temp_array[row-1][col][3];
							 	 array_data[row][col][0] = temp_array[row-1][col][0];
						 		 array_data[row][col][1] -= 30;

					 	 }

			 }
			 v_of_lli++;
		 }
		 	//...........end of abc123............
 }

int theta_calculater_for_increment() // this function when called will give angle where mouse is clicked
{

	float distance_f_pointer_t_ball = Distance( mouse_axis_saving[2], mouse_axis_saving[3], 400, 40);
	float x_axis_distance = Distance( mouse_axis_saving[2], 40, 400, 40);
	float temp = x_axis_distance/distance_f_pointer_t_ball;
	int mno = acos (temp) *180 / 3.14159;
	if (mouse_axis_saving[2]<400){ mno= 180-mno;} //extra lines to get angle from 0 to 180
	return mno;

}
int theta_calculater_for_increment_2() // same purpose only difference is that it is measuring angle by coordinates of mouse moving not clicking, used for pointer
{

	float distance_f_pointer_t_ball = Distance( mouse_axis_saving[0], mouse_axis_saving[1], 400, 40);
	float x_axis_distance = Distance( mouse_axis_saving[0], 40, 400, 40);
	float temp = x_axis_distance/distance_f_pointer_t_ball;
	int mno = acos (temp) *180 / 3.14159;
	if (mouse_axis_saving[0]<400){ mno= 180-mno;} //extra lines to get angle from 0 to 180
	return mno;

}

int theta_calculater_for_increment_1() // same purpose ,of theta_calculater,only difference is that it is measuring angle by coordinates of mouse moving not clicking, used for pointer
{

	float distance_f_pointer_t_ball = Distance( mouse_axis_saving[0], mouse_axis_saving[1], 400, 40);
	float x_axis_distance = Distance( mouse_axis_saving[0], 40, 400, 40);
	float temp = x_axis_distance/distance_f_pointer_t_ball;
	int mno = acos (temp) *180 / 3.14159;
	return mno;

}

int theta_calculater() // this function when called will give angle where mouse is clicked
{
	float distance_f_pointer_t_ball = Distance( mouse_axis_saving[2], mouse_axis_saving[3], 400, 40);
	float x_axis_distance = Distance( mouse_axis_saving[2], 40, 400, 40);
	float temp = x_axis_distance/distance_f_pointer_t_ball;
	int mno = acos (temp) *180 / 3.14159;
	return mno;
}


void calculate_distances () // this function calculate increments of loops for ball which will shoot when mouse clicked
{
	increments_ball_shoot[1] = (mouse_axis_saving[3]-40); //Y-axis increment

	//.......123....... system calculating increments in X-axis
	if (theta_calculater_for_increment()<90)
											{increments_ball_shoot[0] = (mouse_axis_saving[2]-400);}
	else if (theta_calculater_for_increment()>90)
											{increments_ball_shoot[0] = (400-mouse_axis_saving[2]);}
	else if (theta_calculater_for_increment()==90)
											{increments_ball_shoot[0] =0;}
	//.....end of 123.....
}

int GAME_END_Calculater()  // This function will change Game_Code array to 1 so shooting will stop
{
	for (int row=0;row<10;row++)
	{	for (int col=0;col<13;col++)
		{
			if (array_data[row][col][0] != 138  && array_data[row][col][2] < 140)
			{
				Game_Code [0]=1;
				return 1;
			}
		}
	}
	return 0;
}


void Move_Bubble()
{
	int temp_angle=theta_calculater_for_increment();
	float m =  ( increments_ball_shoot[0]);
	float n = (increments_ball_shoot[1]);

	float temp3= (m/n)*5;
	float temp4= (1)*5;
	wall_collision =0;
	float col =400;
	for (float row=40; row <600  ; row+=temp4)
		{
		global_fix1_1=0;
			temp_array[0]=col;
			temp_array[1]=row;
			///....123... this function is checking whether the moving ball y axis is greater then 570 then it will stop the ball and move color to nearest white ball and change moving ball color to white
			int last = last_collision(row);
			if (last == 1)
				{
				Call_Burst();
				//.........1233....... this setup is increasing level
				if (v_of_lli_usedin_mouseclicked%6==5)
				{
					level_line_increase();
				}
				v_of_lli_usedin_mouseclicked ++;
				GAME_END_Calculater();
				//......end of 1233.......
					break;
				}
			/// .... end of 123 ....
			///.........12365........collision detection
			int temp7 = Collision_Detection (); // Collision function if return 1 then it means balls collide so i put break command to break loop for further movement of ball
			if (temp7 == 1)
				{
				    Call_Burst();
				    for (int m=0;m<99;m++)
				    {
				    	cout << array_compare[m] << "," ;
				    }
					//.........1233....... this setup is increasing level
					if (v_of_lli_usedin_mouseclicked%6==5)
					{
						level_line_increase();
					}
					v_of_lli_usedin_mouseclicked ++;
					GAME_END_Calculater();
					//......end of 1233.......
					break;
				}

			Display();
			///.....end of 12365.....

			///......765...... after ball collide to wall these increments will apply
			if (col > 780 || col < 30.1) wall_collision +=1;
			if (wall_collision %2 !=0)
			{
				if (temp_angle>90)
							{col += temp3;}
				else col -=temp3;
			}
			///....end of 765.....

			///......543...... before ball collide to wall, these increments will apply
			else
			{
				if (temp_angle>90)
					{col-= temp3;}
				else col+=temp3;
			}
			///....end of 543....

		}
}
void Call_Burst()  /// this function call the burst function by checking conditions
{
	int n = 0;

	for (int i=0; i<3;i++)
	{
		if (i==0)  // These checking run only first time
		{
			Burst(row_global,col_global, n);
			}


		//Further checking are held by else
		else
		{
			for (int xe=0;xe<100;xe+=2)
			{
				int mn = xe;
				for (int xi=xe ;xi>0 ; xi-=2)
				{

					int u = array_compare[mn-xi];
					int v = array_compare[mn-xi+1];
					Burst(u,v,n);
				}
			}
		}
	}

		if (n >= 6)   //Turning bursted ball to white
		{

			for (int i=0;i<n;i+=2)
			{
				array_data[array_compare[i]][array_compare[i+1]][0] =WHITE;
				SCORE_1 += 50;
				Display();
			}
		}
}


void Burst(int gol_row,int gol_col,int &n) // Function will store tile index in array_compare
{

	for (int row =0;row<10;row++)
	{
		for (int col=0;col<13;col++)
		{
			int Dis = Distance (array_data[row][col][1],array_data[row][col][2],array_data[gol_row][gol_col][1],array_data[gol_row][gol_col][2]); //Calculating Distance between Distance Between Neighborhood Balls

			if (Dis<70 && array_data[row][col][0]==array_data[gol_row][gol_col][0])
			{
				int temp10=0;
										for (int x=0;x<99;x+=2) // this loop check while the compared value already exists in array_compare or not
										{						// if it exist temp10 will become 1 and this value will not add again in array_compare
											if (array_compare [x] ==row && array_compare [x+1] == col)
											{
												temp10=1;
											}
										}
										if (temp10 ==0) //if paired value doesnot exist then it will store it in new indexes
										{
											array_compare [n] =row;
											array_compare [n+1] =col;

											n+=2;
										}
			}
		}
	}
}


bool Collision_Detection ()			// first 2 loops are checking each ball if not white then further if statement is checking distance between moving ball and every visible ball,if distance become less
									// then moving ball color will store in global variable (global_color) and then moving ball color will replace by WHITE
{
	int shortest = 99;
	for (int row=0;row < 10; row++)     //loop1
	{
		for (int col=0; col<13; col++)  //loop2
		{
			if (array_data[row][col][0]!=138) //checking ball should be non white
			{
				if (Distance(array_data[row][col][1],array_data[row][col][2],temp_array[0],temp_array[1]) <60)
				{
					int global_color = shoot_ball_data[3];
					shoot_ball_data[3] = 138; // Coloring shooted ball to white

					///.........123........      // following two loops are now finding where the data of moving ball should be shifted to INVISIBLE(WHITE) state ball
					for (int row=0;row<10;row++) //loop3
					{
						for (int col=0;col<13;col++) //loop4
						{
							int temp_distance = Distance(array_data[row][col][1],array_data[row][col][2],temp_array[0],temp_array[1]);
							if (temp_distance < shortest)   // this condition is checking the closest WHITE ball
							{
								shortest = temp_distance;
								row_global= row;
								col_global= col;
							}
						}
					}
					//from the above loops it will give row and col of tile which is nearest to collision point (note row and col already have there axis(pixelpoint) on index 1 and 2 in array_data[][][];
					array_data[row_global][col_global][0] = global_color;
					global_fix1_1 =1;		// global_fix1_1 is fixing the white ball of collision , it is doing that when it becomes 1, DrawCircle of MovingBall Line couldn't be execute (located in Display); its value will become 0 in Function MOVE_BUBBLE so Display function will again Display movement of Ball
					return 1;
					///......end of 123...
				}
			}
		}

	}
	return 0;

}

bool last_collision(int row) 			/// this function is checking whether the moving ball y axis is greater then 570 then it will stop the ball and move color to nearest white ball and change moving ball color to white
{									// actually this function is same as collision but it is without condition of check non white ball, so it will store color to nearest white ball when y axis of moving ball is equal to 570
	if (row == 570)
	{
		int shortest =99;
		int global_color = shoot_ball_data[3];
		shoot_ball_data[3] = 138; // Coloring shooted ball to white

		///.........123........      // following two loops are now finding where the data of moving ball should be shifted to INVISIBLE(WHITE) state ball
		for (int row=0;row<10;row++) //loop3
		{
			for (int col=0;col<13;col++) //loop4
			{
				int temp_distance = Distance(array_data[row][col][1],array_data[row][col][2],temp_array[0],temp_array[1]);
				if (temp_distance < shortest)   // this condition is checking the closest WHITE ball
				{
					shortest = temp_distance;
					row_global= row;
					col_global= col;
				}
			}
		}
		array_data[row_global][col_global][0] = global_color;
		global_fix1_1 =1;
		Display();
		return 1;
	}
	return 0;
}


void ball_line_initiaizer() // this function call only one time by main function, this function will create store colors of three balls loaded in bar
{
		for (int i=0;i<3;i++)
		{
			ball_line_array[i] = GetRandInRange(1, 10) * 10;
		}
		ball_line_array[3] = 0; //ball changer counter limit to 3
}

int  ball_line_increment() //this function will transfer colors of loaded ball and grenade new ball in index 0 and return the ball color of index 2
{
	int temp = ball_line_array[2] ;
	ball_line_array[2]=ball_line_array[1];
	ball_line_array[1]=ball_line_array[0];
	ball_line_array[0] = GetRandInRange(1, 10) * 10;

	return temp;
}
void Display()/**/{
// set the background color using function glClearColor.
// to change the background play with the red, green and blue values below.
// Note that r, g and b values must be in the range [0,1] where 0 means dim red and 1 means pure red and so on.

	glClearColor(1/*Red Component*/, 1.0/*Green Component*/,
			1.0/*Blue Component*/, 0 /*Alpha component*/); // Red==Green==Blue==1 --> White Colour
	glClear(GL_COLOR_BUFFER_BIT); //Update the colors

// #----------------- Write your code here ----------------------------#
//write your drawing commands here or call your drawing functions...

	//DrawRectangle((width - 30) / 2, 500, 10, 30, colors[GREEN_YELLOW]);


	//...Ball line of shooting

		DrawCircle(345, 40, 25, colors[ball_line_array[2]]);
		DrawCircle(295, 40, 25, colors[ball_line_array[1]]);
		DrawCircle(245, 40, 25, colors[ball_line_array[0]]);
		DrawString(5, 25, "Ball Left: " + Num2Str(ball_change_counter), colors[BLUE_VIOLET]);

		DrawLine (355,67,235,67,3,colors[DARK_GRAY]);
		DrawLine (385,13,235,13,3,colors[DARK_GRAY]);
	//...end of Ball line shooting


	for (int y=0; y<10; y+=2) //loop drawing even no of balls
	{
		for (int x=0; x<13; x++)
		{
			DrawCircle(array_data[y][x][1], array_data[y][x][2], 30, colors[array_data[y][x][0]]);
		}
	}
	for (int y=1; y<10; y+=2) //loop drawing odd no of balls
	{
		for (int x=0; x<13; x++)
		{
			DrawCircle(array_data[y][x][1], array_data[y][x][2], 30, colors[array_data[y][x][0]]);
		}
	}
	if (global_fix1_1 == 0)  // Check last lines of Collision detection function: All detail are given
	{
		DrawCircle(temp_array[0], temp_array[1], 30, colors[shoot_ball_data[3]]); //moving ball
	}
	//.......Draw Pointer
	int x3,y3,angle_1 = theta_calculater_for_increment_1();
	if (theta_calculater_for_increment_2() >90 )
	{
		x3 = 400 -( 100 * cos ((angle_1 * 3.14)/180));
	}
	else
	{
		x3 = 400 +( 100 * cos ((angle_1 * 3.14)/180));
	}
	y3 = 40  + (100* sin ((angle_1)*3.14/180));
	DrawLine (400,40,x3,y3,50,colors[BLUE]);
	///....end pointer

	DrawCircle(400, 40, 30, colors[shoot_ball_data[2]]); // color of this index changed by mouse click event

	DrawRectangle(350, 0, 100, 10, colors[GREEN_YELLOW]);
	DrawString(5, 5, "Score " + Num2Str(SCORE_1), colors[BLUE_VIOLET]);
	if (Game_Code[0] ==1)
		{
			DrawString(380, 300, "Game Over", colors[BLACK]);
		}
	if (Display_message){
	DrawString(365, 300, "Welcome!", colors[BLACK]);
	DrawString(300, 250, "Press Left click to shoot", colors[BLACK]);
	DrawString(280, 200, "Press Right click to skip ball", colors[BLACK]);
	}

// #----------------- Write your code till here ----------------------------#
	// do not modify below this
	glutSwapBuffers(); // do not modify this line..
}
/* Function sets canvas size (drawing area) in pixels...
 *  that is what dimensions (x and y) your game will have
 *  Note that the bottom-left coordinate has value (0,0) and top-right coordinate has value (width-1,height-1)
 * */
void SetCanvasSize(int width, int height) {
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(0, width, 0, height, -1, 1); // set the screen size to given width and height.
}

/*This function is called (automatically) whenever any non-printable key (such as up-arrow, down-arraw)
 * is pressed from the keyboard
 *
 * You will have to add the necessary code here when the arrow keys are pressed or any other key is pressed...
 *
 * This function has three argument variable key contains the ASCII of the key pressed, while x and y tells the
 * program coordinates of mouse pointer when key was pressed.
 *
 * */

void NonPrintableKeys(int key, int x, int y) {
	if (key == GLUT_KEY_LEFT /*GLUT_KEY_LEFT is constant and contains ASCII for left arrow key*/) {
// what to do when left key is pressed...

	} else if (key == GLUT_KEY_RIGHT /*GLUT_KEY_RIGHT is constant and contains ASCII for right arrow key*/) {

	} else if (key == GLUT_KEY_UP/*GLUT_KEY_UP is constant and contains ASCII for up arrow key*/) {
	} else if (key == GLUT_KEY_DOWN/*GLUT_KEY_DOWN is constant and contains ASCII for down arrow key*/) {
	}

	/* This function calls the Display function to redo the drawing. Whenever you need to redraw just call
	 * this function*/
	/*
	 glutPostRedisplay();
	 */
}
/*This function is called (automatically) whenever your mouse moves within inside the game window
 *
 * You will have to add the necessary code here for finding the direction of shooting
 *
 * This function has two arguments: x & y that tells the coordinate of current position of move mouse
 *
 * */

void MouseMoved(int x, int y) {

	mouse_axis_saving[0]=x;
	mouse_axis_saving[1]=height-y;


	cout << "Current Mouse Coordinates X=" << x << " Y= " << height - y<< endl;
	glutPostRedisplay();
}

/*This function is called (automatically) whenever your mouse button is clicked witin inside the game window
 *
 * You will have to add the necessary code here for shooting, etc.
 *
 * This function has four arguments: button (Left, Middle or Right), state (button is pressed or released),
 * x & y that tells the coordinate of current position of move mouse
 *
 * */

void MouseClicked(int button, int state, int x, int y) {

	if (button == GLUT_LEFT_BUTTON) // dealing only with left button
	{

		if (state == GLUT_DOWN && gamestate == Ready)
		{
			cout << "Left Mouse Button Pressed at Coordinates X=" << x << " Y= "
					<< height - y<<endl;
			Display_message = 0;
			temp11[0]=0;
			mouse_axis_saving[2]=x;
			mouse_axis_saving[3]=height -y;
			calculate_distances ();
			comaprison_initializer();

			if (Game_Code[0] == 0)  //Game End checker
			{
				shoot_ball_data[3]=shoot_ball_data[2]; // Transferring color to index of 3 which will keep it save until it will collide with some ball
				shoot_ball_data[2]= ball_line_increment(); // after shooting ball, this index is saving new color of next ball
				Move_Bubble();
			}


		}


	} else if (button == GLUT_RIGHT_BUTTON) // dealing with right button
	{
		Display_message = 0;
		ball_line_array[3] +=1; //This
		if (ball_line_array[3] < 8 && ball_line_array[3]%2==0) // this condition inused because 1 right is not 1, it is 2, so when  even no of ball_line_array[3] come then it will execute :P
     	{
		shoot_ball_data[2]=ball_line_increment();
		ball_change_counter--;
		}



	}
	glutPostRedisplay();
}

/*This function is called (automatically) whenever any printable key (such as x,b, enter, etc.)
 * is pressed from the keyboard
 * This function has three argument variable key contains the ASCII of the key pressed, while x and y tells the
 * program coordinates of mouse pointer when key was pressed.
 * */
void PrintableKeys(unsigned char key, int x, int y) {
	if (key == KEY_ESC/* Escape key ASCII*/) {
		exit(1); // exit the program when escape key is pressed.
	}
}

/*
 * This function is called after every 1000.0/FPS milliseconds
 * (FPS is defined on in the beginning).
 * You can use this function to animate objects and control the
 * speed of different moving objects by varying the constant FPS.
 *
 * */
float dt = 0, lastframe = 0;
void Timer(int m) {
	dt = (m - lastframe) / 1000.0;
	lastframe = m;

	// dt is time elapsed between two frames..

	// Do not modify this line...
	glutTimerFunc(1000.0 / FPS, Timer, m + 1);
}

/*
 * our gateway main function
 * */

void comaprison_initializer() //this function fill all array with 15, this array store the tiles no of color matched
{
	for (int b = 0; b <99; b++)
	{
		array_compare[b] =15;
	}
}

int main(int argc, char*argv[]) {
	InitRandomizer(); // seed the random number generator...
	setcolor = GetColor();
	glutInit(&argc, argv); // initialize the graphics library...
	comaprison_initializer();
	store_balls_coordinates();	// calling function to store coordinates at 1,2 indexes
	store_color_array();		// calling function to store color at 0 index
	shoot_ball_data[2]= GetRandInRange(1, 10) * 10; // storing color of shoot ball, this stores the color only once when game start, after that new color will be stored by mouse click event
	initializing_states();
	ball_line_initiaizer();
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA); // we will be using color display mode
	glutInitWindowPosition(50, 50); // set the initial position of our window
	glutInitWindowSize(width, height); // set the size of our window
	glutCreateWindow("Student bursting machine"); // set the title of our game window
	SetCanvasSize(width, height); // set the number of pixels...

// Register your functions to the library,
// you are telling the library names of function to call for different tasks.
	glutDisplayFunc(Display); // tell library which function to call for drawing Canvas.
	glutSpecialFunc(NonPrintableKeys); // tell library which function to call for non-printable ASCII characters
	glutKeyboardFunc(PrintableKeys); // tell library which function to call for printable ASCII characters


	glutPassiveMotionFunc(MouseMoved); // Mouse
	glutMouseFunc(MouseClicked);
// This function tells the library to call our Timer function after 1000.0/FPS milliseconds...
	glutTimerFunc(1000.0 / FPS, Timer, 0);

// now handle the control to library and it will call our registered functions when
// it deems necessary...
	glutMainLoop();

	return 1;
}
#endif /* */

